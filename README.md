# README #

This is the readme for RSA-ID-Tools - A basic tool for working with RSA Id

### What is this repository for? ###

* A basic tool for working with RSA Id
* Version 1.0

### Current Features
* Simply enter the first 12 digits, and calculate the control digit. This is useful for generating dummy ID's and nothing much else.

### Possible Upcoming Features 
* Validate 13 digit ID
* Select DOB & Gender